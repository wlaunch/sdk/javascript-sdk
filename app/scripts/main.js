/* version 0.21.0 */

(function (root, factory) {
  'use strict';
  /* istanbul ignore next */
  if (typeof define === 'function' && define.amd) {
    var b64encode = function (str) {
      return root.btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    };
    define(['xmlhttprequest', 'js-base64'], function () {
      // THis code fix widget file upload
      return (root.Github = factory(window.XMLHttpRequest, b64encode));
    });
  } else if (typeof module === 'object' && module.exports) {
    if (typeof window !== 'undefined') { // jscs:ignore
      module.exports = factory(window.XMLHttpRequest, window.btoa);
    } else { // jscs:ignore
      module.exports = factory(require('xmlhttprequest').XMLHttpRequest, require('js-base64').Base64.encode);
    }
  } else {
    var b64encode = function (str) {
      return root.btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    };
    root.Wapi = factory(root.XMLHttpRequest, b64encode);
  }
}(this, function (XMLHttpRequest, b64encode) {
  'use strict';
  var Wapi = function (startOptions) {
    var localWapi = {};
    var options = startOptions;
    var API_URL = options.apiUrl + '/v1'; // 'v1' is current version of api and sdk
    var _request = localWapi._request = function _request(method, path, data, cb, raw, sync) {
      function getURL() {
        var url = API_URL + path;

        //url += ((/\?/).test(url) ? '&' : '?');

        if (data && typeof data === 'object' && ['GET', 'HEAD', 'DELETE'].indexOf(method) > -1) {
          for (var param in data) {
            if (data.hasOwnProperty(param))
              url += '&' + encodeURIComponent(param) + '=' + encodeURIComponent(data[param]);
          }
        }

        return url;
      }

      var xhr = new XMLHttpRequest();
      if(raw && method=="GET") xhr.responseType = "arraybuffer";

      xhr.open(method, getURL(), !sync);

      if (!sync) {
        xhr.onreadystatechange = function () {
          if (this.readyState === 4) {
            if (this.status >= 200 && this.status < 300 || this.status === 304) {
              if(this.responseType=="arraybuffer"){
                cb(null, this.response, this);
              }else{
                cb(null, this.responseText ? JSON.parse(this.responseText) : true, this);
                //cb(null, this.responseText ? (this.getResponseHeader('content-type')=="application/octet-stream;charset=UTF-8" ? this.response : JSON.parse(this.responseText)) : true, this);
              }
            } else {
              var reSend = function () {
                _request(method, path, data, cb, raw, sync);
              };
              cb({
                path: path,
                request: this,
                reSend: reSend,
                error: this.status
              });
            }
          }
        };
      }

      if (!raw) {
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
      } else {
        //xhr.setRequestHeader('Content-Type', undefined);
      }

      if ((options.token) || (options.username && options.password)) {
        var authorization = options.token ? 'Bearer ' + options.token :
        'Basic ' + b64encode(options.username + ':' + options.password);

        xhr.setRequestHeader('Authorization', authorization);
      }
      if (raw && data) {
        xhr.send(data);
      } else if (data) {
        xhr.send(JSON.stringify(data));
      } else {
        xhr.send();
      }

      if (sync) {
        return xhr.response;
      }
    };
    var _requestAllPages = localWapi._requestAllPages = function _requestAllPages(path, cb) {
      var results = [];

      (function iterate() {
        _request('GET', path, null, function (err, res, xhr) {
          if (err) {
            return cb(err);
          }

          results.push.apply(results, res);

          var links = (xhr.getResponseHeader('link') || '').split(/\s*,\s*/g);
          var next = null;

          links.forEach(function (link) {
            next = /rel="next"/.test(link) ? link : next;
          });

          if (next) {
            next = (/<(.*)>/.exec(next) || [])[1];
          }

          if (!next) {
            cb(err, results);
          } else {
            path = next;
            iterate();
          }
        });
      })();
    };
    var _urlParams = function (params) {
      if (params) {
        var urlParams = [];
        for (var key in params) {
          if (params.hasOwnProperty(key) && params[key] !== "") {
            if (typeof params[key] === 'object' && !Array.isArray(params[key])) {
              for (var obj in params[key]) {
                urlParams.push(key + "[" + obj + "]" + "=" + params[key][obj]);
              }
            } else {
              urlParams.push(key + "=" + encodeURIComponent(params[key]));
            }
          }
        }
        return '?' + urlParams.join('&');
      } else {
        return '';
      }
    };


    localWapi.setOptions = function (newOptions) {
      options = newOptions;
    };

    localWapi.getPromise = function (callFn, params, cb) {
      return new Promise(function (resolve, reject) {
        localWapi[callFn](params, function(e, result) {
          if (e) {
            reject(e);
            cb && cb(e);
          } else {
            resolve(result);
            cb && cb(undefined, result);
          }
        })
      });
    };

    // "/company"

    /*create Company
     */
    localWapi.createCompany = function (params, cb) {
      _request('POST', '/company', params.body, cb);
    };

    // "/company/{companyId}"

    /*get Company by id
     */
    localWapi.getCompany = function (params, cb) {
      _request('GET', '/company/' + params.companyId + _urlParams(params.params), null, cb);
    };

    /*update Company by id
     */
    localWapi.updateCompany = function (params, cb) {
      _request('POST', '/company/' + params.companyId, params.body, cb);
    };

    // "/company/{companyId}/booking"

    /*update Company Booking Settings
     */
    localWapi.updateCompanyBookingSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/booking', params.body, cb);
    };

    /*get Company Booking Settings
     */
    localWapi.getCompanyBookingSettings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/booking' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/booking"

    /*update Branch Booking Settings
     */
    localWapi.updateBranchBookingSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/booking', params.body, cb);
    };

    /*get Branch Booking Settings
     */
    localWapi.getBranchBookingSettings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/booking' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/appointment"

    /*get Company Appointment with paging and
     filters(branch: Comma separated branch IDs ,
     skipBranch: Comma separated branch IDs to skip)
     */
    localWapi.getCompanyAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/appointment' + _urlParams(params.params), null, cb);
    };

    /*get Company Appointment by id
     */
    localWapi.getCompanyAppointmentByID = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/appointment/' + params.appointmentId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/appointment"

    /*get Branch Appointments, with paging and
     filters(start: Filter start time in UTC (NotNull),
     end: Filter end time in UTC (NotNull),
     id: Comma separated IDs,
     skipId:Comma separated IDs to skip,
     client: Comma separated client IDs,
     skipClient: Comma separated branch IDs to skip,
     service: Comma separated service IDs,
     skipService: Comma separated service IDs to skip,
     resource: Comma separated resource IDs,
     skipResource: Comma separated resource IDs to skip,
     group: Comma separated group IDs,
     skipGroup: Comma separated group IDs to skip,
     status: Comma separated status,
     skipStatus: Comma separated status to skip,
     source: Comma separated source,
     skipSource: Comma separated source to skip)
     */
    localWapi.getBranchAppointments = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment' + _urlParams(params.params), null, cb);
    };

    /*create Branch Appointment
     */
    localWapi.createBranchAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/appointment/{appointmentId}"

    /*get Branch Appointment by id
     */
    localWapi.getBranchAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment/' + params.appointmentId + _urlParams(params.params), null, cb);
    };

    /*update Branch Appointment
     */
    localWapi.updateBranchAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment/' + params.appointmentId + _urlParams(params.params), params.body, cb);
    };


    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/appointment"

    /*get Branch Resource Appointments, with paging and
     filters(start: Filter start time in UTC (NotNull),
     end: Filter end time in UTC (NotNull),
     id: Comma separated IDs,
     skipId:Comma separated IDs to skip,
     client: Comma separated client IDs,
     skipClient: Comma separated branch IDs to skip,
     service: Comma separated service IDs,
     skipService: Comma separated service IDs to skip,
     resource: Comma separated resource IDs,
     skipResource: Comma separated resource IDs to skip,
     group: Comma separated group IDs,
     skipGroup: Comma separated group IDs to skip,
     status: Comma separated status,
     skipStatus: Comma separated status to skip,
     source: Comma separated source,
     skipSource: Comma separated source to skip)
     */
    localWapi.getBranchResourceAppointments = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/appointment' + _urlParams(params.params), null, cb);
    };

    /*create Branch Resource Appointment
     */

    /* body is {appointment:{
     Instant startTime,
     Integer capacity,
     UUID companyId,
     UUID branchId,
     CompanyClientDTO client :{
     String first_name,
     String last_name,
     String phone,
     String email,
     UUID id (if client is exist only this param)
     },
     List<CompanyAppointmentServiceResourceSettingsDTO> serviceResourceSettings:[{
     UUID service;
     Set<UUID> resources;
     Integer ordinal;
     Integer duration;
     Integer priceAmount;
     }],
     String currency,
     String groupId,
     String status,
     String source,
     String comment,
     }}
     */

    localWapi.createBranchResourceAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/appointment' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/appointment/{appointmentId}"

    /*get Branch Resource Appointment by id
     */
    localWapi.getBranchResourceAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/appointment/' + params.appointmentId + _urlParams(params.params), null, cb);
    };

    /*update Branch Resource Appointment
     */
    localWapi.updateBranchResourceAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/appointment/' + params.appointmentId + _urlParams(params.params), params.body, cb);
    };

    /*get Branch Appointment Orders
     */
    localWapi.getBranchAppointmentOrders = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment/' + params.appointmentId + '/client/order'+ _urlParams(params.params), null, cb);
    };

    /*get Branch Resource Appointment Orders
     */
    localWapi.getBranchResourceAppointmentOrders = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId +'/appointment/' + params.appointmentId + '/client/order'+ _urlParams(params.params), null, cb);
    };

    // Rating

      //company

    /*company get user rating list
      */
    // "/company/{companyId}/rating"
    localWapi.getCompanyRatings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/rating'+ _urlParams(params.params), null, cb);
    };

    /*company get user rating by ID
      */
    // "/company/{companyId}/rating/{ratingId}"
    localWapi.getCompanyRating = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/rating/' + params.ratingId + _urlParams(params.params), null, cb);
    };

    /*company update user rating by ID
     */
    // "/company/{companyId}/rating/{ratingId}"
    localWapi.updateCompanyRating = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/rating/' + params.ratingId + _urlParams(params.params), params.body, cb);
    };

      //branch

    /*company branch get user rating list
      */
    // "/company/{companyId}/branch/{branchId}/rating"
    localWapi.getCompanyBranchRatings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/rating'+ _urlParams(params.params), null, cb);
    };

    /*company branch get user rating by ID
      */
    // "/company/{companyId}/branch/{branchId}/rating/{ratingId}"
    localWapi.getCompanyBranchRating = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/rating/' + params.ratingId + _urlParams(params.params), null, cb);
    };

    /*company branch update user rating by ID
     */
    // "/company/{companyId}/branch/{branchId}/rating/{ratingId}"
    localWapi.updateCompanyBranchRating = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/rating/' + params.ratingId + _urlParams(params.params), params.body, cb);
    };

      //resource

    /*company branch resource get user rating list
      */
    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/rating"
    localWapi.getCompanyBranchResourceRatings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/rating'+ _urlParams(params.params), null, cb);
    };

    /*company branch resource get user rating by ID
      */
    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/rating/{ratingId}"
    localWapi.getCompanyBranchResourceRating = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/rating/' + params.ratingId + _urlParams(params.params), null, cb);
    };

    /*public get ratings by company
      */
    // "/public/company/{companyId}/rating"
    localWapi.getPublicCompanyResourceRating = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/rating' + _urlParams(params.params), null, cb);
    };

    /*public get ratings by company branch
      */
    // "/public/company/{companyId}/branch/{branchId}/rating"
    localWapi.getPublicCompanyBranchResourceRating = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/rating' + _urlParams(params.params), null, cb);
    };

      //user

    /*user send rating appointment resource
      */
    // "/user/company/{companyId}/appointment/{appointmentId}/rating"
    localWapi.userSendRatingAppointment = function (params, cb) {
      _request('POST', '/user/company/' + params.companyId + '/appointment/' + params.appointmentId + '/rating' , params.body, cb);
    };

    // "/user/{userId}/appointment"

    /*get User Appointments, with paging and
     params( start : start time filter (required),
     end : end time filter (required))
     */
    localWapi.getUserAppointments = function (params, cb) {
      _request('GET', '/user/appointment' + _urlParams(params.params), null, cb);
    };

    /*create User Appointment
     */
    localWapi.createUserAppointment = function (params, cb) {
      _request('POST', '/user/appointment', params.body, cb);
    };

    // "/user/{userId}/appointment/{appointmentId}"

    /*get User Appointment by id
     */
    localWapi.getUserAppointment = function (params, cb) {
      _request('GET', '/user/appointment/' + params.appointmentId, null, cb);
    };

    /*update User Appointment by user
     */
    localWapi.updateUserAppointment = function (params, cb) {
      _request('POST', '/user/appointment/' + +params.appointmentId, params.body, cb);
    };

    // "/user/appointment/{appointmentId}/cancel"
    /*cancel any company user appointment
     */

    localWapi.cancelUserAppointmentByID = function (params, cb) {
      _request('POST', '/user/appointment/' + params.appointmentId + '/cancel' + _urlParams(params.params), params.body, cb);
    };

    // "/user/appointment/{appointmentId}"
    /*update any company user appointment
     */

    localWapi.updateUserAppointmentByID = function (params, cb) {
      _request('POST', '/user/appointment/' + params.appointmentId + _urlParams(params.params), params.body, cb);
    };

    // "/client/appointment"

    /*create Client Appointment
     */
    localWapi.createClientAppointment = function (params, cb) {
      _request('POST', '/client/appointment' + _urlParams(params.params), params.body, cb);
    };

    // "/client/group_training/{groupTrainingId}/appointment";

    /*create Client groupTraining Appointment
     */
    localWapi.createClientGroupTrainingAppointment = function (params, cb) {
      _request('POST', '/client/group_training/' + params.groupTrainingId + '/appointment' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch"

    /*get Branches, with paging and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     createdStart: Resource created start time filter in UTC,
     createdEnd: Resource created end time filter in UTC,
     condition: Condition applied to name, middleName, lastName, lastName, phone and email. Can be "and" or "or". Default is "and",
     name:Comma separated resource name tokens,
     phone:Comma separated resource phone tokens,
     email:Comma separated resource email tokens)
     */
    localWapi.getBranches = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch' + _urlParams(params.params), null, cb);
    };

    /*create Branch
     */
    localWapi.createBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}"

    /*get Branch by id
     */
    localWapi.getBranch = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + _urlParams(params.params), null, cb);
    };

    /*update Branch
     */
    localWapi.updateBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch_group"

    /*get Branches Group, with paging
     */
    localWapi.getBranchesGroup = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch_group' + _urlParams(params.params), null, cb);
    };

    /*create Branch Group
     */
    localWapi.createBranchGroup = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch_group', params.body, cb);
    };

    // "/company/{companyId}/branch_group/{branchGroupId}"

    /*get Branch Group by id
     */
    localWapi.getBranchGroup = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch_group/' + params.branchGroupId, null, cb);
    };

    /*update Branch Group
     */
    localWapi.updateBranchGroup = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch_group/' + params.branchGroupId, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service"

    /* get Branch Services,
     with paging and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     name: Comma separated service name tokens,
     duration: Comma separated service duration tokens)
     */
    localWapi.getBranchServices = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/{serviceId}"

    /* get Branch Services by Id
     */
    localWapi.getBranchService = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + _urlParams(params.params), null, cb);
    };

    /*update Branch
     */
    localWapi.updateBranchService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId, params.body, cb);
    };

    /*add Service to Branch
     */
    localWapi.addServiceToBranch = function (params, cb) {
      _request('PUT', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId, params.body, cb);
    };

    /*remove Service to Branch
     */
    localWapi.removeServiceToBranch = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId, params.body, cb);
    };

    /*get entity export
     */
    localWapi.getExport = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/' + params.entity + '/export' + _urlParams(params.params), null, cb, true);
    };

    /* post entity import
     */
    localWapi.postImport = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/' + params.entity + '/import', params.body, cb, true);
    };

    // "/company/{companyId}/resource"

    /* get Resources,
     with paging,
     with params(withServices : return services object (!required),
     withServiceSettings : return services settings object (!required)) and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     createdStart: Resource created start time filter in UTC,
     createdEnd: Resource created end time filter in UTC,
     condition: Condition applied to name, middleName, lastName, lastName, phone and email. Can be "and" or "or". Default is "and",
     name:Comma separated resource name tokens,
     middleName:Comma separated resource middle name tokens,
     lastName:Comma separated resource last name tokens,
     phone:Comma separated resource phone tokens,
     email:Comma separated resource email tokens)
     */
    localWapi.getResources = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource{resourceId}"

    /* get Resource by id
     */
    localWapi.getResource = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId, null, cb);
    };

    // "/user/change_password"

    /* change user password by id
     */
    localWapi.changePassword = function (params, cb) {
      _request('POST', '/user/change_password', params.body, cb);
    };

    /* update Resource by id
     */
    localWapi.updateResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource"

    /* get Branch Resources,
     with paging,
     with params(withServices : return services object (!required),
     withServiceSettings : return services settings object (!required)) and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     createdStart: Resource created start time filter in UTC,
     createdEnd: Resource created end time filter in UTC,
     condition: Condition applied to name, middleName, lastName, lastName, phone and email. Can be "and" or "or". Default is "and",
     name:Comma separated resource name tokens,
     middleName:Comma separated resource middle name tokens,
     lastName:Comma separated resource last name tokens,
     phone:Comma separated resource phone tokens,
     email:Comma separated resource email tokens,
     */
    localWapi.getBranchResources = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource' + _urlParams(params.params), null, cb);
    };

    /*create Branch Resource
     */
    localWapi.createBranchResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}"

    /* get Branch Resource by Id
     */
    localWapi.getBranchResource = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + _urlParams(params.params), null, cb);
    };

    /*update Branch Resource
     */
    localWapi.updateBranchResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/service/attach"

    /*attach Service to Branch Resource
     */
    localWapi.attachServiceToBranchResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/service/attach', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/service/detach"

    /* detach Service from Branch Resource
     */
    localWapi.detachServiceFromBranchResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/service/detach', params.body, cb);
    };

    // "/company/{companyId}/service/{serviceId}/resource/attach"

    /*attach Company Resource to Service
     */
    localWapi.attachCompanyResourceToService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/' + params.serviceId + '/resource/attach', params.body, cb);
    };

    // "/company/{companyId}/service/{serviceId}/resource/detach"

    /* detach Company Resource from Service
     */
    localWapi.detachCompanyResourceFromService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/' + params.serviceId + '/resource/detach', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/{serviceId}/resource/attach"

    /*attach Branch Resource to Service
     */
    localWapi.attachBranchResourceToService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + '/resource/attach', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/{serviceId}/resource/detach"

    /* detach Branch Resource from Service
     */
    localWapi.detachBranchResourceFromService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + '/resource/detach', params.body, cb);
    };

    // "/public/company/{companyId}/resource/{resourceId}/branch"

    /*get public Resource Branches , with paging
     */
    localWapi.getPublicResourceBranches = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/resource/' + params.resourceId + '/branch' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/branch"

    /*get Resource Branches , with paging
     */
    localWapi.getResourceBranches = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/branch' + _urlParams(params.params), null, cb);
    };

    /*create Resource Branch
     */
    localWapi.createResourceBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId + '/branch', params.body, cb);
    };

    // "/public/company/{companyId}/resource/{resourceId}/branch/{id}"

    /*get public Resource Branch by id
     */
    localWapi.getPublicResourceBranch = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/resource/' + params.resourceId + '/branch/' + params.id, null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/branch/{id}"

    /*get Resource Branch by id
     */
    localWapi.getResourceBranch = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/branch/' + params.id, null, cb);
    };

    /*update Resource Branch
     */
    localWapi.updateResourceBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId + '/branch/' + params.id, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource_group"

    /*get Branches Resources Group, with paging
     */
    localWapi.getBranchResourcesGroup = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource_group' + _urlParams(params.params), null, cb);
    };

    /*create Branch Resource Group
     */
    localWapi.createBranchResourceGroup = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource_group', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource_group/{resourceGroupId}"

    /*get Branch Resource Group by id
     */
    localWapi.getBranchResourceGroup = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource_group/' + params.resourceGroupId, params.body, cb);
    };

    /*update Branch Resource Group
     */
    localWapi.updateBranchResourceGroup = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource_group/' + params.resourceGroupId, params.body, cb);
    };

    // "/schedule/{scheduleId}"

    /*get Schedule by id, with params(
     start: Filter start time in ISO 8601, Example: "2011-12-03" (required),
     end: Filter start time in ISO 8601, Example: "2011-12-03" (required))
     */
    localWapi.getSchedule = function (params, cb) {
      _request('GET', '/schedule/' + params.scheduleId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/schedule"

    /*get Branch Schedule by id, with params(
     start: Filter start time in ISO 8601, Example: "2011-12-03" (required),
     end: Filter start time in ISO 8601, Example: "2011-12-03" (required))
     */
    localWapi.getBranchSchedule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/schedule' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/schedule"

    /*get Branches Resources Schedule,
     with paging and
     params(start : start time filter (required),
     end : end time filter (required))
     */
    localWapi.getResourceSchedule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/schedule' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/schedule"

    /*get Branches Resources Schedule,
     with paging and
     params(start : start time filter (required),
     end : end time filter (required))
     */
    localWapi.getBranchResourceSchedule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/schedule' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/schedule/cycle"

    /*create Branch Schedule Cycle
     */
    localWapi.createBranchScheduleCycle = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/schedule/cycle', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/schedule/cycle/{frameId}"

    /*update Branch Schedule Cycle
     */
    localWapi.updateBranchScheduleCycle = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/schedule/cycle/' + params.id, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/schedule/day"

    /*create Branch Schedule Day
     */
    localWapi.createBranchScheduleDay = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/schedule/day', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/schedule/day/{frameId}"

    /*update Branch Schedule Day
     */
    localWapi.updateBranchScheduleDay = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/schedule/day/' + params.id, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/schedule/cycle"

    /*create Branch Resource Schedule Cycle
     */
    localWapi.createBranchResourceScheduleCycle = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/schedule/cycle', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/schedule/cycle/{frameId}"

    /*update Branch Resource Schedule Cycle
     */
    localWapi.updateBranchResourceScheduleCycle = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/schedule/cycle/' + params.id, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/schedule/day"

    /*create Branch Resource Schedule Day
     */
    localWapi.createBranchResourceScheduleDay = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/schedule/day', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/schedule/day/{frameId}"

    /*update Branch Resource Schedule Day
     */
    localWapi.updateBranchResourceScheduleDay = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/schedule/day/' + params.id, params.body, cb);
    };

    // "/company/{companyId}/resource_type"

    /*get Resource Type, with paging
     */
    localWapi.getResourceTypes = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource_type' + _urlParams(params.params), null, cb);
    };

    /*create Resource Type
     */
    localWapi.createResourceType = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource_type', params.body, cb);
    };

    // "/company/{companyId}/resource_type/{resourceTypeId}"

    /*get Resource Type by id
     */
    localWapi.getResourceType = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource_type/' + params.resourceTypeId, null, cb);
    };

    /*update Resource Type
     */
    localWapi.updateResourceType = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource_type/' + params.resourceTypeId, params.body, cb);
    };

    // "/company/{companyId}/service/price_list"

    /*get service Price List, with paging and
     params(withBranches : return branches object (!required))
     */
    localWapi.getServicePriceList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/price_list' + _urlParams(params.params), null, cb);
    };

    /*create service Price List
     */
    localWapi.createServicePriceList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/price_list', params.body, cb);
    };

    // "/company/{companyId}/service/price_list/{priceListId}"

    /*get service Price List by id, with params(
     withBranches : return branch object (!required),
     */
    localWapi.getOneServicePriceList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/price_list/' + params.priceListId + _urlParams(params.params), null, cb);
    };

    /*update service Price List
     */
    localWapi.updateServicePriceList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/price_list/' + params.priceListId, params.body, cb);
    };

    // "/company/{companyId}/service/price_list/{priceListId}/price"

    /*get Price from service Price List, with paging
     */
    localWapi.getServicePrices = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/price_list/' + params.priceListId + '/price' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service/price_list/{priceListId}/price/{priceId}"

    /*get Price by id from service price list
     */
    localWapi.getServicePrice = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/price_list/' + params.priceListId + '/price/' + params.priceId, null, cb);
    };

    // "/company/{companyId}/service/prices"

    /*create service Price
     */
    localWapi.createServicePrice = function (params, cb) {
      _request('PUT', '/company/' + params.companyId + '/service/prices', params.body, cb);
    };

    /*update service Price
     */
    localWapi.updateServicePrice = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/prices', params.body, cb);
    };

    // "/company/{companyId}/service/{serviceId}/prices"

    /*get Service Price, with paging
     */
    localWapi.getPricesFromService = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/' + params.serviceId + '/prices' + _urlParams(params.params), null, cb);
    };

    // "/company"

    /*get Companies, with paging
     */
    localWapi.getCompanies = function (params, cb) {
      _request('GET', '/company' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/price_list/{priceListId}"

    /*add Price List to Branch
     */
    localWapi.addServicePriceListToBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/price_list/' + params.priceListId, params.body, cb);
    };

    /*remove Price List from Branch
     */
    localWapi.removeServicePriceListFromBranch = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/price_list/' + params.priceListId, null, cb);
    };

    // "/company/{companyId}/client"

    /*get Company Clients, with paging and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     createdStart: Resource created start time filter in UTC,
     createdEnd: Resource created end time filter in UTC,
     condition: Condition applied to name, middleName, lastName, lastName, phone and email. Can be "and" or "or". Default is "and",
     name:Comma separated resource name tokens,
     middleName:Comma separated resource middle name tokens,
     lastName:Comma separated resource last name tokens,
     phone:Comma separated resource phone tokens,
     email:Comma separated resource email tokens,
     */
    localWapi.getCompanyClients = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client' + _urlParams(params.params), null, cb);
    };

    /*create Client
     */
    localWapi.createClient = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/client', params.body, cb);
    };

    // "/company/{companyId}/client/limited"

    /*get Company Clients limited with
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     createdStart: Resource created start time filter in UTC,
     createdEnd: Resource created end time filter in UTC,
     condition: Condition applied to name, middleName, lastName, lastName, phone and email. Can be "and" or "or". Default is "and",
     name:Comma separated resource name tokens,
     middleName:Comma separated resource middle name tokens,
     lastName:Comma separated resource last name tokens,
     phone:Comma separated resource phone tokens,
     email:Comma separated resource email tokens,
     */
    localWapi.getCompanyClientsLimited = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/limited' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/client/{clientId}"

    /*get Client by id
     */
    localWapi.getCompanyClient = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/' + params.clientId + _urlParams(params.params), null, cb);
    };

    /*update Client
     */
    localWapi.updateClient = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/client/' + params.clientId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/frame/resource"

    /*get Branch Frames, with filters(
     start: Filter start time in ISO 8601, Example: "2011-12-03" (NotNull),
     end: Filter start time in ISO 8601, Example: "2011-12-03" (NotNull),
     id: Comma separated resource IDs (NotEmpty))
     */
    localWapi.getFrames = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/frame/resource' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service"

    /* get Services,
     with paging and
     filters(id: Comma separated IDs ,
     skipId:Comma separated IDs to skip,
     name: Comma separated service name tokens,
     duration: Comma separated service duration tokens)
     */
    localWapi.getServices = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service' + _urlParams(params.params), null, cb);
    };

    /*create Service
     */
    localWapi.createService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service', params.body, cb);
    };

    // "/company/{companyId}/service/{serviceId}"

    /*get Service by id
     */
    localWapi.getService = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/' + params.serviceId  + _urlParams(params.params), null, cb);
    };

    /*update Service
     */
    localWapi.updateService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/' + params.serviceId, params.body, cb);
    };

    // "/me"

    /*get Current User
     */
    localWapi.getUserInfo = function (params, cb) {
      _request('GET', '/me', null, cb);
    };

    // "/me/company"

    //TODO: remove this
    /*get Current User Frames, with filters(
     start: Filter start time in ISO 8601, Example: "2011-12-03" (NotNull),
     end: Filter start time in ISO 8601, Example: "2011-12-03" (NotNull),
     id: Comma separated resource IDs (NotEmpty))
     */
    localWapi.getUserCompany = function (params, cb) {
      _request('GET', '/me/company' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/user"

    /* get Company Users, with paging
     */
    localWapi.getCompanyUsers = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/user/{userId}"

    /* get Company User by Id
     */
    localWapi.getCompanyUser = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user/' + params.userId, null, cb);
    };

    // "/company/{companyId}/user/{userId}"

    /* delete Company User by Id
     */
    localWapi.deleteCompanyUser = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/user/' + params.userId, null, cb);
    };

    // "/company/{companyId}/user/{userId}/settings"

    /* update Company User Settings by Id
     */
    localWapi.updateCompanyUserSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/' + params.userId + '/settings', params.body, cb);
    };

    // "/company/{companyId}/user/{userId}/bo/settings"

    /* update Company User Settings by Id
     */
    localWapi.updateCompanyUserBoSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/' + params.userId + '/bo/settings', params.body, cb);
    };

    // "/company/{companyId}/file"

    /* upload Company File
     */
    localWapi.uploadCompanyFile = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/file', params.body, cb, true);
    };

    // "/company/{companyId}/image/file"

    /* upload Company image File
     */
    localWapi.uploadCompanyImageFile = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/image/file' + _urlParams(params.params), params.body, cb, true);
    };

    // "/user/{userId}/file"

    /* upload User File
     */
    localWapi.uploadUserFile = function (params, cb) {
      _request('POST', '/user/' + params.userId + '/file', params.body, cb, true);
    };

    // "/user/verify/{token}"

    /* verify User by Token
     */
    localWapi.verifyUserToken = function (params, cb) {
      _request('POST', '/user/verify/' + params.token, null, cb);
    };

    // "/user/email/verify"
    /* verify user email
     */
    localWapi.verifyUserEmail = function (params, cb) {
      _request('POST', '/user/email/verify/', params.body, cb);
    };

    // "/user/phone/verify"
    /* verify user phone
     */
    localWapi.verifyUserPhone = function (params, cb) {
      _request('POST', '/user/phone/verify/', params.body, cb);
    };

    // "/company/{companyId}/user/email/invitation"

    /*  invitation new user by email
     */
    localWapi.invitationUserEmail = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/email/invitation', params.body, cb);
    };

    // "/company/{companyId}/user/sms/invitation"

    /*  invitation new user by sms
     */
    localWapi.invitationUserSms = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/sms/invitation', params.body, cb);
    };

    // "/user/email/invitation/accept"

    /* user invitation email accept
     */
    localWapi.userInvitationEmailAccept = function (params, cb) {
      _request('POST', '/user/email/invitation/accept', params.body, cb);
    };

    // "/user/sms/invitation/accept"

    /* user invitation sms accept
     */
    localWapi.userInvitationSmsAccept = function (params, cb) {
      _request('POST', '/user/sms/invitation/accept', params.body, cb);
    };

    // "/user/email/invitation/decline"

    /* user invitation email decline
     */
    localWapi.userInvitationEmailDecline = function (params, cb) {
      _request('POST', '/user/email/invitation/decline', params.body, cb);
    };

    // "/user/sms/invitation/decline"

    /* user invitation sms decline
     */
    localWapi.userInvitationSmsDecline = function (params, cb) {
      _request('POST', '/user/sms/invitation/decline', params.body, cb);
    };

    // "/user/email/invitation/check"

    /* user invitation email decline
     */
    localWapi.userInvitationEmailCheck = function (params, cb) {
      _request('POST', '/user/email/invitation/check', params.body, cb);
    };

    // "/user/sms/invitation/check"

    /* user invitation sms decline
     */
    localWapi.userInvitationSmsCheck = function (params, cb) {
      _request('POST', '/user/sms/invitation/check', params.body, cb);
    };

    // "/user/{id}"

    /* get user
     */
    localWapi.getUser = function (params, cb) {
      _request('GET', '/user/' + params.id, null, cb);
    };

    // "/user/{id}/company"

    /* get user
     */
    localWapi.getUserCompanies = function (params, cb) {
      _request('GET', '/user/' + params.id+'/company' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service/{serviceId}/order/change"

    /* update service order
     */
    localWapi.serviceOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/' + params.serviceId + '/order/change', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/order/change"

    /* update resource order
     */
    localWapi.resourceOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/order/change', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/order/change"

    /* update branch order
     */
    localWapi.branchOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/order/change', params.body, cb);
    };

    // /company/{companyId}/type/{typeCode}

    /* update company type
     */
    localWapi.changeCompanyType = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/type/' + params.type, params.body, cb);
    };

    // /licence

    /* get licences
     */
    localWapi.getLicenseList = function (params, cb) {
      _request('GET', '/licence' + _urlParams(params.params), null, cb);
    };

    // /licence/{licenceId}

    /* get licence by ID
     */
    localWapi.getLicense = function (params, cb) {
      _request('GET', '/licence/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/message"

    /*get mailing history
     */
    localWapi.getMailingHistory = function (params, cb) {
      _request('GET', '/message' + _urlParams(params.params), null, cb);
    };

    // "/message{messageId}"

    /*get mailing history message
     */
    localWapi.getMessage = function (params, cb) {
      _request('GET', '/message/' + params.messageId + _urlParams(params.params), null, cb);
    };

    // /payment

    /* get all paymet
      */

    localWapi.getPaymentList = function (params, cb) {
      _request('GET', '/payment' + _urlParams(params.params), null, cb);
    };

    // /payment/{paymentId}

    /* get paymet by ID
     */
    localWapi.getPayment = function (params, cb) {
      _request('GET', '/payment/' + params.id + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/payment

    /* get company paymets
      */

    localWapi.getCompanyPaymentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/payment/{paymentId}

    /* get company paymet by ID
     */
    localWapi.getCompanyPayment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment/' + params.id + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/payment/{paymentId}/process

    localWapi.processCompanyPayment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payment/' + params.id + '/process', params.body, cb);
    };

    // /company/{companyId}/payment/provider/stripe/setup

    localWapi.setupCompanyStripeProvider = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payment/provider/stripe/setup', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}payment/provider/stripe/setup

    localWapi.setupCompanyBranchStripeProvider = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/provider/stripe/setup', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/payment

    // get branch company paymets

    localWapi.getBranchCompanyPaymentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/payment/{paymentId}

    // get branch company payment by ID

    localWapi.getBranchCompanyPayment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/' + params.id + _urlParams(params.params), null, cb);
    };


    // /company/{companyId}/branch/{branchId}/payment/{paymentId}/process

    localWapi.processBranchCompanyPayment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/' + params.id + '/process', params.body, cb);
    };

    // DECRECATED
    localWapi.liqpayOrderLicense = function (params, cb) {
      params.body.provider = 'LIQPAY';
      _request('POST', '/payment/company/' + params.companyId + '/licence', params.body, cb);
    };
    // DECRECATED
    localWapi.liqpayOrderSms = function (params, cb) {
      params.body.provider = 'LIQPAY';
      _request('POST', '/payment/company/' + params.companyId + '/sms', params.body, cb);
    };
    // DECRECATED
    localWapi.liqpayOrderEmail = function (params, cb) {
      params.body.provider = 'LIQPAY';
      _request('POST', '/payment/company/' + params.companyId + '/email', params.body, cb);
    };
    // DECRECATED
    localWapi.liqpayBalanceAdd = function (params, cb) {
      params.body.provider = 'LIQPAY';
      _request('POST', '/payment/company/' + params.companyId + '/balance', params.body, cb);
    };
    // DECRECATED
    localWapi.getLiqpayPaymentStatus = function (params, cb) {
      params.body.provider = 'LIQPAY';
      _request('GET', '/payment/company/' + params.companyId + '/status/payment/' + params.id + _urlParams(params.params), null, cb);
    };
    // DECRECATED
    localWapi.balanceOrderLicense = function (params, cb) {
      params.body.provider = 'BALANCE';
      _request('POST', '/payment/company/' + params.companyId + '/licence', params.body, cb);
    };
    // DECRECATED
    localWapi.balanceOrderSms = function (params, cb) {
      params.body.provider = 'BALANCE';
      _request('POST', '/payment/company/' + params.companyId + '/sms', params.body, cb);
    };
    // DECRECATED
    localWapi.balanceOrderEmail = function (params, cb) {
      params.body.provider = 'BALANCE';
      _request('POST', '/payment/company/' + params.companyId + '/email', params.body, cb);
    };

    /* Сreate payment actions
     */
    localWapi.paymentOrderLicense = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/licence', params.body, cb);
    };
    localWapi.paymentOrderSms = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/sms', params.body, cb);
    };
    localWapi.paymentOrderEmail = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/email', params.body, cb);
    };
    localWapi.paymentBalanceAdd = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/balance', params.body, cb);
    };
    localWapi.getPaymentStatus = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment/' + params.id + _urlParams(params.params), null, cb);
    };
    localWapi.createMembershipPayment = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/membership/' + params.membershipId + _urlParams(params.params), params.body, cb);
    };
    localWapi.createAppointmentPayment = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/appointment/' + params.appointmentId + _urlParams(params.params), params.body, cb);
    };
    localWapi.updatePayment = function (params, cb) {
      _request('POST', '/payment/company/' + params.companyId + '/appointment/payment/' + params.paymentId + _urlParams(params.params), params.body, cb);
    };

    // branch payment/operation

    /* Simple create payment operation using cashdesk and amount
     */
    localWapi.createBranchPaymentOperationSimple = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/operation/simple' + _urlParams(params.params), params.body, cb);
    };
    /* Simple create payment operation by resource using cashdesk and amount
     */
    localWapi.createBranchResourcePaymentOperationSimple = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/payment/operation/simple' + _urlParams(params.params), params.body, cb);
    };
    /* Create payment operation by the document
     */
    localWapi.createBranchPaymentOperationDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/operation/document' + _urlParams(params.params), params.body, cb);
    };
    /* Update payment operation
     */
    localWapi.updateBranchPaymentOperation = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/operation/' + params.operationId + _urlParams(params.params), params.body, cb);
    };
    /* Update resource payment operation
     */
    localWapi.updateBranchResourcePaymentOperation = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/payment/operation/' + params.operationId + _urlParams(params.params), params.body, cb);
    };
    /* Get payment operation by id
     */
    localWapi.getBranchPaymentOperation = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/operation/' + params.operationId + _urlParams(params.params), null, cb);
    };
    /* Get resource payment operation by id
     */
    localWapi.getBranchResourcePaymentOperation = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/payment/operation/' + params.operationId + _urlParams(params.params), null, cb);
    };
    /* Delete payment operation
     */
    localWapi.deleteBranchPaymentOperation = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/operation/' + params.operationId + _urlParams(params.params), null, cb);
    };
    // company payment/operation

    /* Simple create payment operation using cashdesk and amount
     */
    localWapi.createCompanyPaymentOperationSimple = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payment/operation/simple' + _urlParams(params.params), params.body, cb);
    };
    /* Create payment operation by the document
     */
    localWapi.createCompanyPaymentOperationDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payment/operation/document' + _urlParams(params.params), params.body, cb);
    };
    /* Update payment operation
     */
    localWapi.updateCompanyPaymentOperation = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payment/operation/' + params.operationId + _urlParams(params.params), params.body, cb);
    };
    /* GET payment operation by id
     */
    localWapi.getCompanyPaymentOperation = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment/operation/' + params.operationId + _urlParams(params.params), null, cb);
    };
    /* Delete payment operation
     */
    localWapi.deleteCompanyPaymentOperation = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/payment/operation/' + params.operationId + _urlParams(params.params), null, cb);
    };

    // /widget

    /* get widget
     */
    localWapi.getWidget = function (params, cb) {
      _request('GET', '/widget' + _urlParams(params.params), null, cb);
    };

    // /widget/{widgetId}

    /* find widget
     */
    localWapi.findWidget = function (params, cb) {
      _request('GET', '/widget/' + params.widgetId, null, cb);
    };

    // "/company/{companyId}/user/{userId}"
    /* user update with confirm sms by company
     */
    localWapi.companyUserUpdate = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/' + params.id, params.body, cb);
    };

    // "/user/{id}"

    /* user update
     */
    localWapi.userUpdate = function (params, cb) {
      _request('POST', '/user/' + params.id, params.body, cb);
    };

    // /company/{companyId}/widget

    /* get company widget
     */
    localWapi.getCompanyWidget = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/widget' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/widget

    /* create company widget
     */
    localWapi.createCompanyWidget = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/widget', params.body, cb);
    };

    // /company/{companyId}/widget/{widgetId}

    /* find company widget
     */
    localWapi.findCompanyWidget = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/widget/' + params.widgetId, null, cb);
    };

    // /company/{companyId}/widget/{widgetId}

    /* update company widget
     */
    localWapi.updateCompanyWidget = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/widget/' + params.widgetId, params.body, cb);
    };

    // /company/{companyId}/widget/{widgetId}

    /* delete company widget
     */
    localWapi.deleteCompanyWidget = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/widget/' + params.widgetId, null, cb);
    };

    // /company/{companyId}/widget/{widgetId}/upload

    /* upload widget file
     */
    localWapi.uploadWidgetFile = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/widget/' + params.widgetId + '/upload', params.body, cb, true);
    };

    // /company/{companyId}/widget/{widgetId}/file

    /* get company widget file
     */
    localWapi.getCompanyWidgetFile = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/widget/' + params.widgetId + '/file' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/widget/{widgetId}/file

    /* update company widget file
     */
    localWapi.updateCompanyWidgetFile = function (params, cb) {
      _request('PUT', '/company/' + params.companyId + '/widget/' + params.widgetId + '/file' + _urlParams(params.params), params.body, cb);
    };

    localWapi.updateCompanyWidgetFileForm = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/widget/' + params.widgetId + '/file' + _urlParams(params.params), params.body, cb, true);
    };

    // /company/{companyId}/widget/{widgetId}/file

    /* delete company widget file
     */
    localWapi.deleteCompanyWidgetFile = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/widget/' + params.widgetId + '/file' + _urlParams(params.params), null, cb);
    };

    // /widget/{widgetId}/company/{companyId}/install

    /* install widget by id in company
     */
    localWapi.installWidget = function (params, cb) {
      _request('POST', '/widget/' + params.widgetId + '/company/' + params.companyId + '/install', null, cb);
    };

    // /company/{companyId}/widget/{widgetId}/list

    /* company widget list
     */
    localWapi.getCompanyWidgetList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/widget/' + params.widgetId + '/list' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/widget/{widgetId}/configuration

    /* get company widget configuration
     */
    localWapi.getCompanyWidgetConfiguration = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/widget/' + params.widgetId + '/configuration' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/widget/{widgetId}/configuration

    /* update company widget configuration
     */
    localWapi.updateCompanyWidgetConfiguration = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/widget/' + params.widgetId + '/configuration' + _urlParams(params.params), params.body, cb);
    };

    // get company webhook list
    // "/company/{companyId}/webhook";
    localWapi.getCompanyWebhookList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/webhook' + _urlParams(params.params), null, cb);
    };

    // get company branch webhook list
    // "/company/{companyId}/branch/{branchId}/webhook";
    localWapi.getBranchWebhookList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/webhook' + _urlParams(params.params), null, cb);
    };

    // create company branch webhook
    // "/company/{companyId}/branch/{branchId}/webhook";
    localWapi.createBranchWebhook = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/webhook', params.body, cb);
    };

    // update company branch webhook by ID
    // "/company/{companyId}/branch/{branchId}/webhook/{notificationId}"
    localWapi.updateBranchWebhook = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/webhook/' + params.notificationId, params.body, cb);
    };

    // get company branch webhook by ID
    // "/company/{companyId}/branch/{branchId}/webhook/{notificationId}"
    localWapi.getBranchWebhook = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/webhook/' + params.notificationId + _urlParams(params.params), null, cb);
    };

    // clone branch webhook
    // "/company/{companyId}/branch/{branchId}/webhook/clone
    localWapi.cloneBranchWebhook = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/webhook/clone', params.body, cb);
    };

    // /company/{companyId}/notification

    /* get company notification list
     */
    localWapi.getCompanyNotificationList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/notification' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/notification

    /* get branch notification list
     */
    localWapi.getBranchNotificationList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification' + _urlParams(params.params), null, cb);
    };

    /* create branch notification
     */
    localWapi.createBranchNotificationList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/notification/{notificationId}

    /* get branch notification by id
     */
    localWapi.getBranchNotification = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification/' + params.notificationId + _urlParams(params.params), null, cb);
    };

    /* update branch notification by id
     */
    localWapi.updateBranchNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification/' + params.notificationId, params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/notification/copy

    /* copy branch notification
     */
    localWapi.copyBranchNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification/copy', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/notification/clone

    /* clone branch notification
     */
    localWapi.cloneBranchNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification/clone', params.body, cb);
    };

    // /company/{companyId}/reminder

    /* get company reminder list
     */
    localWapi.getCompanyReminderList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/reminder' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/reminder

    /* get branch reminder list
     */
    localWapi.getBranchReminderList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder' + _urlParams(params.params), null, cb);
    };

    /* create branch reminder
     */
    localWapi.createBranchReminderList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/reminder/{reminderId}

    /* get branch reminder by id
     */
    localWapi.getBranchReminder = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder/' + params.reminderId + _urlParams(params.params), null, cb);
    };

    /* update branch reminder by id
     */
    localWapi.updateBranchReminder = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder/' + params.reminderId, params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/reminder/copy

    /* copy branch reminder
     */
    localWapi.copyBranchReminder = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder/copy', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/reminder/clone

    /* clone branch reminder
     */
    localWapi.cloneBranchReminder = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/reminder/clone', params.body, cb);
    };

    // /company/{companyId}/notification_settings

    /* get company notification settings
     */
    localWapi.getNotificationSettings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/notification_settings', null, cb);
    };
    /* set company notification settings
     */
    localWapi.setNotificationSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/notification_settings', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/notification_settings

    /* get branch notification settings
     */
    localWapi.getBranchNotificationSettings = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification_settings', null, cb);
    };
    /* set branch notification settings
     */
    localWapi.setBranchNotificationSettings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/notification_settings', params.body, cb);
    };

    // /company/{{companyId}}/branch/{{branchId}}/telegram/subscribe
    /* generate branch notification telegram link
     */
    localWapi.generateBranchNotificationTelegramLink = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/telegram/subscribe', params.body, cb);
    };

    // /company/{{companyId}}/resource/{{resourceId}}/telegram/subscribe
    /* generate resource notification telegram link
     */
    localWapi.generateResourceNotificationTelegramLink = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId + '/telegram/subscribe', params.body, cb);
    };

    // /company/{{companyId}}/branch/{{branchId}}/resource/{{resourceId}}/telegram/subscribe
    /* generate branch resource notification telegram link
     */
    localWapi.generateBranchResourceNotificationTelegramLink = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/telegram/subscribe', params.body, cb);
    };

    // /company/{companyId}/client/statistics/day

    /* get company clients statistics day
     */
    localWapi.getCompanyClientsStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/statistics/month

    /* get company clients statistics month
     */
    localWapi.getCompanyClientsStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/statistics/total/day

    /* get company clients total statistics day
     */
    localWapi.getCompanyClientsTotalStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/statistics/total/day' + _urlParams(params.params), null, cb);
    };
    // /company/{companyId}/client/statistics/total/month

    /* get company clients total statistics month
     */
    localWapi.getCompanyClientsTotalStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/statistics/total/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/{clientId}/statistics/day

    /* get company client statistics day
     */
    localWapi.getCompanyClientStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/' + params.clientId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/{clientId}/statistics/month

    /* get company client statistics month
     */
    localWapi.getCompanyClientStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/' + params.clientId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/statistics/day

    /* get company clients statistics day
     */
    localWapi.getCompanyBranchClientsStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/statistics/month

    /* get company clients statistics month
     */
    localWapi.getCompanyBranchClientsStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/statistics/total/day

    /* get company branch clients total statistics day
     */
    localWapi.getCompanyBranchClientsTotalStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/statistics/total/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/statistics/total/month

    /* get company branch clients total statistics month
     */
    localWapi.getCompanyBranchClientsTotalStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/statistics/total/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/{clientId}/statistics/day

    /* get company client statistics day
     */
    localWapi.getCompanyBranchClientStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/' + params.clientId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/{clientId}/statistics/month

    /* get company client statistics month
     */
    localWapi.getCompanyBranchClientStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/' + params.clientId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/statistics/day

    /* get company resources statistics day
     */
    localWapi.getCompanyResourcesStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/statistics/month

    /* get company resources statistics month
     */
    localWapi.getCompanyResourcesStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/statistics/total/day

    /* get company resources total statistics day
     */
    localWapi.getCompanyResourcesTotalStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/statistics/total/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/statistics/total/month

    /* get company resources total statistics month
     */
    localWapi.getCompanyResourcesTotalStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/statistics/total/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/{resourceId}/statistics/day

    /* get company resource statistics day
     */
    localWapi.getCompanyResourceStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/resource/{resourceId}/statistics/month

    /* get company resource statistics month
     */
    localWapi.getCompanyResourceStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/statistics/day

    /* get company branch resources statistics day
     */
    localWapi.getCompanyBranchResourcesStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/statistics/month

    /* get company branch resources statistics month
     */
    localWapi.getCompanyBranchResourcesStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/statistics/total/day

    /* get company branch resources total statistics day
     */
    localWapi.getCompanyBranchResourcesTotalStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/statistics/total/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/statistics/total/month

    /* get company branch resources total statistics month
     */
    localWapi.getCompanyBranchResourcesTotalStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/statistics/total/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/{resourceId}/statistics/day

    /* get company branch resource statistics day
     */
    localWapi.getCompanyBranchResourceStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/{resourceId}/statistics/month

    /* get company branch resource statistics month
     */
    localWapi.getCompanyBranchResourceStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/service/statistics/day

    /* get company services statistics day
     */
    localWapi.getCompanyServicesStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/service/statistics/month

    /* get company services statistics month
     */
    localWapi.getCompanyServicesStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/service/{serviceId}/statistics/day

    /* get company service statistics day
     */
    localWapi.getCompanyServiceStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/' + params.serviceId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/service/{serviceId}/statistics/month

    /* get company service statistics month
     */
    localWapi.getCompanyServiceStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/service/' + params.serviceId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/service/statistics/day

    /* get company branch services statistics day
     */
    localWapi.getCompanyBranchServicesStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/service/statistics/month

    /* get company branch services statistics month
     */
    localWapi.getCompanyBranchServicesStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/service/{serviceId}/statistics/day

    /* get company service statistics day
     */
    localWapi.getCompanyBranchServiceStatisticsDay = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + '/statistics/day' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/service/{serviceId}/statistics/month

    /* get company service statistics month
     */
    localWapi.getCompanyBranchServiceStatisticsMonth = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + '/statistics/month' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/message

    /* send client message
     */
    localWapi.sendMessage = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/client/message' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/resource/branch

    /* get resources branches
     */
    localWapi.getResourcesBranches = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/branch' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/billing"

    /*get company billing info
     */
    localWapi.getCompanyBillingInfo = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/billing' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/field"

    /*get company extra field list
     */
    localWapi.getCompanyExtraFieldList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/field' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/field"

    /* create company extra field
     */
    localWapi.createCompanyExtraField = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/field' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/field/{fieldId}"

    /*get company extra field by id
     */
    localWapi.getCompanyExtraField = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/field/' + params.fieldId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/field/{fieldId}"

    /* update company extra field
     */
    localWapi.updateCompanyExtraField = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/field/' + params.fieldId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/extra/file"

    /* upload custom extra file
     */
    localWapi.uploadCompanyExtraFile = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/extra/file', params.body, cb, true);
    };

    // "/company/{companyId}/extra/file"

    /* upload custom extra file
     */
    localWapi.getCompanyExtraFile = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/extra/file' + _urlParams(params.params), null, cb);
    };

    /* remove custom extra file
     */
    localWapi.removeCompanyExtraFile = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/extra/file' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/membership/template"

    /* create company membership template
     */
    localWapi.createCompanyMembershipTemplate = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/template', params.body, cb);
    };

    /* get company membership templates list
     */
    localWapi.getCompanyMembershipTemplates = function (params, cb) {
      _request('GET', '/company/' + params.companyId + "/membership/template" + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/membership/template/{templateId}"

    /* update company membership template
     */
    localWapi.updateCompanyMembershipTemplate = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/template/' + params.templateId + _urlParams(params.params), params.body, cb);
    };

    /* get company membership template by Id
     */
    localWapi.getCompanyMembershipTemplate = function (params, cb) {
      _request('GET', '/company/' + params.companyId + "/membership/template/" + params.templateId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/membership/template";

    /* get company branch membership templates list
     */
    localWapi.getCompanyBranchMembershipTemplates = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId  + '/membership/template' + _urlParams(params.params), null, cb);
    };

    /* get company branch membership template by Id
     */
    localWapi.getCompanyBranchMembershipTemplate = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId  + '/membership/template/' + params.templateId + _urlParams(params.params), null, cb);
    };

    /* create company branch membership template
     */
    localWapi.createCompanyBranchMembershipTemplate = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/template', params.body, cb);
    };

    /* update company branch membership template
     */
    localWapi.updateCompanyBranchMembershipTemplate = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/template/' + params.templateId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/membership/template/move"
    /* move branch membership templates
     */
    localWapi.branchMembershipTemplateFolderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId +'/membership/template/move', params.body, cb);
    };

    // "/company/{companyId}/membership/template/move"
    /* move company membership templates
     */
    localWapi.companyMembershipTemplateFolderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId +'/membership/template/move', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/membership/template/{templateId}/order/change"
    /* update branch membership template order
     */
    localWapi.branchMembershipTemplateOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/template/' + params.id + '/order/change', params.body, cb);
    };

    // "/company/{companyId}/membership/template/{templateId}/order/change"
    /* update company membership template order
     */
    localWapi.companyMembershipTemplateOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/template/' + params.id + '/order/change', params.body, cb);
    };

    // "/company/{companyId}/user/membership"

    /* get company user memberships
     */
    localWapi.getUserMemberships = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user/membership' + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/membership/template"

    /* get public company membership templates
     */
    localWapi.getPublicCompanyMembershipTemplates = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/membership/template' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/client/template/{templateId}/membership"
    /* public create client membership by template
     */
    localWapi.createMembershipByTemplateWithPaymentData = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/client/template/' + params.templateId + '/membership' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/membership"

    /* create company membership
     */
    localWapi.createCompanyMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership', params.body, cb);
    };

    /* get company membership list
     */
    localWapi.getCompanyMemberships = function (params, cb) {
      _request('GET', '/company/' + params.companyId + "/membership" + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/membership/{membershipId}"

    /* update company membership
     */
    localWapi.updateCompanyMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/' + params.membershipId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/membership/{membershipId}/freeze"

    /* freeze company membership
     */
    localWapi.freezeCompanyMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/' + params.membershipId + '/freeze' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/membership/{membershipId}/unfreeze"

    /* unfreeze company membership
     */
    localWapi.unfreezeCompanyMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/membership/' + params.membershipId + '/unfreeze' + _urlParams(params.params), params.body, cb);
    };

    /* get company membership by Id
     */
    localWapi.getCompanyMembership = function (params, cb) {
      _request('GET', '/company/' + params.companyId + "/membership/" + params.membershipId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/membership";

    /* get company branch membership list
     */
    localWapi.getCompanyBranchMemberships = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId  + '/membership/' + _urlParams(params.params), null, cb);
    };

    /* get company branch membership by Id
     */
    localWapi.getCompanyBranchMembership = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId  + '/membership/' + params.membershipId + _urlParams(params.params), null, cb);
    };

    /* create company branch membership
     */
    localWapi.createCompanyBranchMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership', params.body, cb);
    };

    /* update company branch membership
     */
    localWapi.updateCompanyBranchMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/' + params.membershipId + _urlParams(params.params), params.body, cb);
    };

    /* freeze company branch membership
     */
    localWapi.freezeCompanyBranchMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/' + params.membershipId + '/freeze' + _urlParams(params.params), params.body, cb);
    };

    /* unfreeze company branch membership
     */
    localWapi.unfreezeCompanyBranchMembership = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/' + params.membershipId + '/unfreeze' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/membership/limited

    localWapi.getCompanyBranchMembershipsLimited = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/membership/limited' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/client/{clientId}/membership/limited"

    localWapi.getCompanyClientMembershipsLimited = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/' + params.clientId + '/membership/limited' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/user/membership"

    localWapi.getCompanyUserMemberships = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/user/membership' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service/parent"
    /* change services parent
     */
    localWapi.changeServicesParent = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/parent', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/parent"
    /* change resources parent
     */
    localWapi.changeResourceParent = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/parent', params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/slot/resource

    /* get company branch resource slots
     */

    localWapi.getBranchResourceSlots = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/slot/resource' + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/resource"

    /* get public company Resources
     */
    localWapi.getPublicResources = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/resource' + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/resource{resourceId}"

    /* get Resource by id
     */
    localWapi.getPublicResource = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/resource/' + params.resourceId, null, cb);
    };

    // "/public/company/{companyId}/branch/{branchId}/resource"

    /* get public Branch Resources
     */
    localWapi.getPublicBranchResources = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/resource' + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/branch/{branchId}/resource/{resourceId}"

    /* get public Branch Resource by Id
     */
    localWapi.getPublicBranchResource = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/branch/{branchId}/service"

    /* get Branch Services,
     */
    localWapi.getPublicBranchServices = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/service' + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/branch/{branchId}/service/{serviceId}"

    /* get Branch Services by Id
     */
    localWapi.getPublicBranchService = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/service/' + params.serviceId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training"
    /*
     ** get branch group training list
     */

    localWapi.getBranchGroupTrainingList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training' + _urlParams(params.params), null, cb);
    };

    /*
     ** create branch group training list
     */

    localWapi.createBranchGroupTraining = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}"
    /*
     ** get branch group training by id
     */

    localWapi.getBranchGroupTraining = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + _urlParams(params.params), null, cb);
    };

    // "/public/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}"
    /*
     **  public get branch group training slot by id
     */

    localWapi.getPublicBranchGroupTraining = function (params, cb) {
      _request('GET', '/public/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + _urlParams(params.params), null, cb);
    };

    /*
     ** update branch group training by id
     */

    localWapi.updateBranchGroupTraining = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}/deactivate"

    localWapi.removeBranchGroupTraining = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + '/deactivate', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/slot/gt/resource"

    /* get company branch resource gt slots
     */

    localWapi.getBranchResourceGtSlots = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/slot/gt/resource' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/group_training"
    /*
     ** get branch resource group training list
     */

    localWapi.getBranchResourceGroupTrainingList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/group_training' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/group_training/{groupTrainingId}"
    /*
     ** get branch resource group training list
     */

    localWapi.getBranchResourceGroupTraining = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/group_training/' + params.groupTrainingId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}"

    /*get Company Resource
     */
    localWapi.getCompanyResource = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}"

    /* update Company Resource
     */
    localWapi.updateCompanyResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId, params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}/appointment/change_status"

    /* update GT appointments status
     */
    localWapi.branchGroupTrainingChangeAppointmentStatus = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + '/appointment/change_status'+ _urlParams(params.params), params.body, cb);
    };

    //"/company/{companyId}/branch/{branchId}/resource/{resourceId}/group_training/{groupTrainingId}/appointment/change_status";

    /* update GT appointments status by resource manager
     */
    localWapi.branchResourceGroupTrainingChangeAppointmentStatus = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/group_training/' + params.groupTrainingId + '/appointment/change_status'+ _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}/date"

    /* get available date to clone group_training by id
     */
    localWapi.getAvailableGtDatesToClone = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + "/date" + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/{groupTrainingId}/clone"
    // clone group_training by id
    localWapi.cloneBranchGroupTrainings = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/' + params.groupTrainingId + "/clone" + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/client/clone"
    // clone different GT clients
    localWapi.fillGroupTrainingWithClients = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/client/clone' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/group_training/client/clone"
    // clone different GT clients with resource permitions
    localWapi.fillResourceGroupTrainingWithClients = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/group_training/client/clone' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training"
    /*
     ** update branch group training list
     */

    localWapi.updateBranchGroupTrainingList = function (params, cb) {
      _request('PUT', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/group_training/deactivate"

    /*
     ** deactivate branch group training list
     */

    localWapi.removeBranchGroupTrainingList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/group_training/deactivate' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/appointment/{appointmentId}/date"
    /* get available date to clone appointment by id
     */
    localWapi.getAvailableDatesToCloneAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment/' + params.appointmentId + "/date" + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/appointment/{appointmentId}/clone"
    // clone appointment by id
    localWapi.cloneBranchAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/appointment/' + params.appointmentId + "/clone" + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/branch/{id}"

    /*
     ** deactivate branch group training list
     */

    localWapi.removeCompanyResourceBranch = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/resource/' + params.resourceId + '/branch/' + params.id, null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/schedule/day/clone"

    /*
     ** clone branch schedule day frames
     */

    localWapi.cloneBranchDayScheduleFrame = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/schedule/day/clone', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/schedule/day/clone"

    /*
     ** clone resource schedule day frames
     */

    localWapi.cloneResourceDayScheduleFrame = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/schedule/day/clone', params.body, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/service/attach"

    /*attach Service to Resource
     */
    localWapi.attachServiceToResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId + '/service/attach', params.body, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/service/detach"

    /* detach Service from Resource
     */
    localWapi.detachServiceFromResource = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/resource/' + params.resourceId + '/service/detach', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/attach"

    /* attach Service to Branch
     */
    localWapi.attachServicesToBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/attach', params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/service/detach"

    /* detach Service from Branch
     */
    localWapi.detachServicesFromBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/service/detach', params.body, cb);
    };

    // "/company/{companyId}/product"

    /* get company product list
     */
    localWapi.getCompanyProducts = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/product"

    /* create company product
     */
    localWapi.createCompanyProduct = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/product/{productId}"

    /* get company product by id
     */
    localWapi.getCompanyProduct = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/product/{productId}"

    /* update company product by id
     */
    localWapi.updateCompanyProduct = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse"

    /* get company warehouse list
     */
    localWapi.getCompanyWarehouseList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse"

    /* create company warehouse
     */
    localWapi.createCompanyWarehouse = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/{warehouseId}"

    /* get company warehouse by id
     */
    localWapi.getCompanyWarehouse = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/{warehouseId}"

    /* update company warehouse by id
     */
    localWapi.updateCompanyWarehouse = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse"

    /* get company branch warehouse list
     */
    localWapi.getCompanyBranchWarehouseList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse"

    /* create company branch warehouse
     */
    localWapi.createCompanyBranchWarehouse = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/{warehouseId}"

    /* get company branch warehouse by id
     */
    localWapi.getCompanyBranchWarehouse = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/' + params.warehouseId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/{warehouseId}"

    /* update company branch warehouse by id
     */
    localWapi.updateCompanyBranchWarehouse = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/' + params.warehouseId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/unit";
    // "/company/{companyId}/unit/{unitId}";

    // "/company/{companyId}/unit"

    /* get company unit list
     */
    localWapi.getCompanyUnitList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/unit' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/unit"

    /* create company unit
     */
    localWapi.createCompanyUnit = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/unit' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/unit/{unitId}"

    /* get company unit by id
     */
    localWapi.getCompanyUnit = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/unit/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/unit/{unitId}"

    /* update company unit by id
     */
    localWapi.updateCompanyUnit = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/unit/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/unit_exchange"

    /* get company unit exchange list
     */
    localWapi.getCompanyUnitExchangeList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/unit_exchange' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/unit_exchange"

    /* create company unit exchange
     */
    localWapi.createCompanyUnitExchange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/unit_exchange' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/unit_exchange/{id}"

    /* get company unit exchange by id
     */
    localWapi.getCompanyUnitExchange = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/unit_exchange/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/unit_exchange/{id}"

    /* update company unit exchange
     */
    localWapi.updateCompanyUnitExchange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/unit_exchange/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/unit_exchange/{id}"

    /* remove company unit exchange
     */
    localWapi.removeCompanyUnitExchange = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/unit_exchange/' + params.id, null, cb);
    };

    // "/company/{{companyId}}/warehouse/threshold"

    /* get company warehouse threshold list
     */
    localWapi.getCompanyWarehouseThresholdList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/threshold' + _urlParams(params.params), null, cb);
    };
    /* create company warehouse threshold
     */
    localWapi.createCompanyWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/threshold' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{{companyId}}/warehouse/threshold/{{thresholdId}}"

    /* get company warehouse threshold by id
     */
    localWapi.getCompanyWarehouseThreshold = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/threshold/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{{companyId}}/warehouse/threshold/{{thresholdId}}"

    /* update company warehouse threshold
     */
    localWapi.updateCompanyWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/threshold/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{{companyId}}/warehouse/{{warehouseId}}/threshold/clone"

    /* clone company warehouse threshold
     */
    localWapi.cloneCompanyWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + '/threshold/clone', params.body, cb);
    };

    // "/company/{{companyId}}/branch/{{branchId}}/warehouse/threshold"

    /* get branch warehouse threshold list
     */
    localWapi.getBranchWarehouseThresholdList = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/warehouse/threshold' + _urlParams(params.params), null, cb);
    };

    // "/company/{{companyId}}/branch/{{branchId}}/warehouse/threshold/{{thresholdId}}"

    /* get branch warehouse threshold by id
     */
    localWapi.getBranchWarehouseThreshold = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/threshold/' + params.id + _urlParams(params.params), null, cb);
    };

    // "company/{{companyId}}/branch/{{branchId}}/warehouse/threshold"

    /* create branch warehouse threshold
     */
    localWapi.createBranchWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/threshold' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{{companyId}}/branch/{{branchId}}/warehouse/threshold/{{thresholdId}}"

    /* update branch warehouse threshold
     */
    localWapi.updateBranchWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/threshold/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{{companyId}}/branch/{{branchId}}/warehouse/{{warehouseId}}/threshold/clone"

    /* clone branch warehouse threshold
     */
    localWapi.cloneBranchWarehouseThreshold = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/' + params.warehouseId + '/threshold/clone', params.body, cb);
    };

    // "/company/{{companyId}}/warehouse/{{warehouseId}}/threshold/balance"

    /* get company warehouse threshold balance
     */
    localWapi.getCompanyWarehouseThresholdBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + '/threshold/balance/' + _urlParams(params.params), null, cb);
    };

    // "company/{{companyId}}}/branch/{{branchId}}/warehouse/{{warehouseId}}/threshold/balance"

    /* get branch warehouse threshold balance
     */
    localWapi.getBranchWarehouseThresholdBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/' + params.warehouseId + '/threshold/balance/' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/tech_map"

    /* get company techMap list
     */
    localWapi.getCompanyTechMapList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tech_map' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/tech_map"

    /* get company resource techMap list
     */
    localWapi.getCompanyResourceTechMapList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/tech_map' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/tech_map

    /* create company techMap
     */
    localWapi.createCompanyTechMap = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tech_map' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}"

    /* get company techMap by id
     */
    localWapi.getCompanyTechMap = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tech_map/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}"

    /* update company techMap
     */
    localWapi.updateCompanyTechMap = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tech_map/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}/details"

    /* get company techMap details list
     */
    localWapi.getCompanyTechMapDetailsList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tech_map/' + params.techMapId + '/details' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/{resourceId}/tech_map/{techMapId}/details""

    /* get company resource techMap details list
     */
    localWapi.getCompanyResourceTechMapDetailsList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/' + params.resourceId + '/tech_map/' + params.techMapId + '/details' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}/details"

    /* create company techMap details
     */
    localWapi.createCompanyTechMapDetails = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tech_map/' + params.techMapId + '/details' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}/details/{detailsId}"

    /* get company techMap details by id
     */
    localWapi.getCompanyTechMapDetails = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tech_map/' + params.techMapId + '/details/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}/details/{detailsId}"

    /* update company techMap details
     */
    localWapi.updateCompanyTechMapDetails = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tech_map/' + params.techMapId + '/details/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tech_map/{techMapId}/details/{detailsId}"

    /* remove company techMap details
     */
    localWapi.removeCompanyTechMapDetails = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/tech_map/' + params.techMapId + '/details/' + params.id, null, cb);
    };

    // "/company/{companyId}/warehouse/document"

    /* get company stock document list
     */
    localWapi.getCompanyStockDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/document' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document"

    /* get company branch stock document list
     */
    localWapi.getBranchStockDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/warehouse/document' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/document/{stockDocumentId}"

    /* get company stock document
     */
    localWapi.getCompanyStockDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/document/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/{stockDocumentId}"

    /* get company branch stock document
     */
    localWapi.getBranchStockDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/' + params.id + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/warehouse/document/transfer

    /* create company stock document type transfer
     */
    localWapi.createTransferCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/transfer' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/warehouse/document/transfer

    /* create company branch stock document type transfer
     */
    localWapi.createTransferBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/transfer' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/warehouse/document/transfer

    /* get company stock document list type transfer
     */
    localWapi.getTransferCompanyStockDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/document/transfer' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/transfer"

    /* get company branch stock document list type transfer
     */
    localWapi.getTransferBranchStockDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/warehouse/document/transfer' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/document/income"

    /* create company stock document type income
     */
    localWapi.createIncomeCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/income' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/income"

    /* create company branch stock document type income
     */
    localWapi.createIncomeBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/income' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/outcome"

    /* create company stock document type outcome
     */
    localWapi.createOutcomeCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/outcome' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/outcome"

    /* create company branch stock document type outcome
     */
    localWapi.createOutcomeBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/outcome' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/inventory"

    /* create company stock document type inventory
     */
    localWapi.createInventoryCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/inventory' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/inventory"

    /* create company branch stock document type inventory
     */
    localWapi.createInventoryBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/inventory' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/transfer/{documentId}"

    /* update company stock document type transfer
     */
    localWapi.updateTransferCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/transfer/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/transfer/{documentId}"

    /* update company branch stock document type transfer
     */
    localWapi.updateTransferBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/transfer/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/transfer/{documentId}"

    localWapi.getTransferCompanyStockDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/document/transfer/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/transfer/{documentId}"

    localWapi.getTransferBranchStockDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/transfer/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/document/income/{stockDocumentId}"

    /* update company stock document type income
     */
    localWapi.updateIncomeCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/income/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/income/{stockDocumentId}"

    /* update company branch stock document type income
     */
    localWapi.updateIncomeBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/income/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/outcome/{stockDocumentId}"

    /* update company stock document type outcome
     */
    localWapi.updateOutcomeCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/outcome/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/outcome/{stockDocumentId}"

    /* update company branch stock document type outcome
     */
    localWapi.updateOutcomeBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/outcome/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/document/inventory/{stockDocumentId}"

    /* update company stock document type inventory
     */
    localWapi.updateInventoryCompanyStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/warehouse/document/inventory/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/document/inventory/{stockDocumentId}"

    /* update company branch stock document type inventory
     */
    localWapi.updateInventoryBranchStockDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/document/inventory/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/warehouse/{warehouseId}/product/{productId}/balance"

    /* get warehouse product balance
     */
    localWapi.getWarehouseProductBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + '/product/' + params.productId + '/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/warehouse/{warehouseId}/balance"

    /* get warehouse balance
     */
    localWapi.getBranchWarehouseBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/warehouse/' + params.warehouseId + '/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/{warehouseId}/balance"

    /* get warehouse balance
     */
    localWapi.getWarehouseBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/' + params.warehouseId + '/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/product/{productId}/balance"

    /* get product balance
     */
    localWapi.getProductBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/' + params.productId + '/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/warehouse/product/price"

    /* get company product prices balance
     */
    localWapi.getCompanyProductPricesBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/warehouse/product/price' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service/{serviceId}/tech_map/{techMapId}/attach"

    /* attach TechMap to Service
     */
    localWapi.attachTechMapService = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/service/' + params.serviceId + '/tech_map/' + params.techMapId + '/attach' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/service/{serviceId}/tech_map/{techMapId}/detach"

    /* detach TechMap from Service
     */
    localWapi.detachTechMapService = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/service/' + params.serviceId + '/tech_map/' + params.techMapId + '/detach' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk"

    /* get company cash desk list
     */
    localWapi.getCompanyCashDeskList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/cash_desk' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk/{cashDeskId}"

    /* get company cash desk
     */
    localWapi.getCompanyCashDesk = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/cash_desk/' + params.cashDeskId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk"

    /* create company cash desk
     */
    localWapi.createCompanyCashDesk = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/cash_desk' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk/{cashDeskId}"

    /* update company cash desk
     */
    localWapi.updateCompanyCashDesk = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/cash_desk/' + params.cashDeskId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk"

    /* get company cash desk list
     */
    localWapi.getCompanyBranchCashDeskList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk"

    /* get company cash desk
     */
    localWapi.getCompanyBranchCashDesk = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk/' + params.cashDeskId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk"

    /* create company cash desk
     */
    localWapi.createCompanyBranchCashDesk = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk/{cashDeskId}"

    /* update company cash desk
     */
    localWapi.updateCompanyBranchCashDesk = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk/' + params.cashDeskId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk/document"

    /* get company cash desk document list
     */
    localWapi.getCompanyCashDeskDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/cash_desk/document' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/cash_desk/document"

    /* get company branch desk document list
     */
    localWapi.getBranchCashDeskDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/cash_desk/document' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/cash_desk/document/{cashDeskDocumentId}"

    /* get company cash desk document
     */
    localWapi.getCompanyCashDeskDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/cash_desk/document/' + params.cashDeskDocumentId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/cash_desk/document/{cashDeskDocumentId}"

    /* get company branch cash desk document
     */
    localWapi.getBranchCashDeskDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk/document/' + params.cashDeskDocumentId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk/document"

    /* create company cash desk document
     */
    localWapi.createCompanyCashDeskDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/cash_desk/document' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/cash_desk/document"

    /* create company branch cash desk document
     */
    localWapi.createBranchCashDeskDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk/document' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/cash_desk/document/{cashDeskDocumentId}"

    /* update company stock document
     */
    localWapi.updateCompanyCashDeskDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/cash_desk/document/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/cash_desk/document/{cashDeskDocumentId}"

    /* update company branch cash desk document
     */
    localWapi.updateBranchCashDeskDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/cash_desk/document/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/message"

    /* get company mailing history
     */
    localWapi.getCompanyMailingHistory = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/message' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/message/{messageId}"

    /* get company message by ID
     */
    localWapi.getCompanyMessage = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/message/' + params.messageId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/message/{messageId}/resend"

    /* company message resend
     */
    localWapi.resendCompanyMessage = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/message/' + params.messageId + '/resend' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/message"

    /* get company branch mailing history
     */
    localWapi.getBranchMailingHistory = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId + '/message' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/message/{messageId}"

    /* get company branch message by ID
     */
    localWapi.getBranchMessage = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/message/' + params.messageId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/message/{messageId}/resend"

    /* company branch message resend
     */
    localWapi.resendBranchMessage = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/message/' + params.messageId + '/resend' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/payroll_operation_type"
    // create company payroll operation type
    localWapi.createPayrollOperationType = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payroll_operation_type' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/payroll_operation_type"
    // get company payroll operation types
    localWapi.getPayrollOperationTypeList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payroll_operation_type' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/payroll_operation_type/{operationTypeId}"
    // get company payroll operation type by ID
    localWapi.getPayrollOperationType = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payroll_operation_type/' + params.operationTypeId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/payroll_operation_type/{operationTypeId}"
    // update company payroll operation type by ID
    localWapi.updatePayrollOperationType = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payroll_operation_type/' + params.operationTypeId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/payroll/document"
    // create company payroll document
    localWapi.createPayrollDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payroll/document' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/payroll/document"
    // get company payroll document
    localWapi.getPayrollDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payroll/document' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/payroll/document/{documentId}"
    // get company payroll document by ID
    localWapi.getPayrollDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payroll/document/' + params.documentId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/payroll/document/{documentId}"
    // update company payroll document by ID
    localWapi.updatePayrollDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/payroll/document/' + params.documentId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payroll/document"
    // create branch payroll document
    localWapi.createBranchPayrollDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payroll/document' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payroll/document"
    // get branch payroll document
    localWapi.getBranchPayrollDocumentList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payroll/document' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payroll/document/{documentId}"
    // get branch payroll document by ID
    localWapi.getBranchPayrollDocument = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payroll/document/' + params.documentId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payroll/document/{documentId}"
    // update branch payroll document by ID
    localWapi.updateBranchPayrollDocument = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payroll/document/' + params.documentId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/product/price_list"

    /*get product Price List, with paging and
     params(withBranches : return branches object (!required))
     */
    localWapi.getProductPriceList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/price_list' + _urlParams(params.params), null, cb);
    };

    /*create product Price List
     */
    localWapi.createProductPriceList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product/price_list', params.body, cb);
    };

    // "/company/{companyId}/product/price_list/{priceListId}"

    /*get product Price List by id, with params(
     withBranches : return branch object (!required),
     */
    localWapi.getOneProductPriceList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/price_list/' + params.priceListId + _urlParams(params.params), null, cb);
    };

    /*update product Price List
     */
    localWapi.updateProductPriceList = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product/price_list/' + params.priceListId, params.body, cb);
    };

    // "/company/{companyId}/product/prices"

    /*create product Price
     */
    localWapi.createProductPrice = function (params, cb) {
      _request('PUT', '/company/' + params.companyId + '/product/prices', params.body, cb);
    };

    /*update product Price
     */
    localWapi.updateProductPrice = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product/prices', params.body, cb);
    };

    // "/company/{companyId}/product/{productId}/prices"

    /*get product Price, with paging
     */
    localWapi.getPricesFromProduct = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/' + params.productId + '/prices' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/product/price_list/{priceListId}/price"

    /*get product Price, with paging
     */
    localWapi.getProductPrices = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/product/price_list/' + params.priceListId + '/price/' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/product/price_list/{priceListId}"

    /*add product Price List to Branch
     */
    localWapi.addProductPriceListToBranch = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/product/price_list/' + params.priceListId, params.body, cb);
    };

    /*remove product Price List from Branch
     */
    localWapi.removeProductPriceListFromBranch = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/branch/' + params.branchId + '/product/price_list/' + params.priceListId, null, cb);
    };

    // "/company/{companyId}/product/price_list/{priceListId}/price/{priceId}"

    localWapi.removeProductPrice = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/product/price_list/' + params.priceListId + '/price/' + params.priceId, null, cb);
    };

    // "/company/{companyId}/counterparty/limited"
    // get limited company counterparty list
    localWapi.getCompanyCounterpartyLimitedList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/counterparty/limited' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/counterparty"

    /*get company counterparty list, with paging
     */
    localWapi.getCompanyCounterpartyList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/counterparty' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/counterparty"

    /*create company counterparty
     */
    localWapi.createCompanyCounterparty = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/counterparty' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/counterparty/{counterpartyId}"

    /*get company counterparty, by id
     */
    localWapi.getCompanyCounterparty = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/counterparty/' + params.counterpartyId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/counterparty/{counterpartyId}"

    /*update company counterparty
     */
    localWapi.updateCompanyCounterparty = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/counterparty/' + params.counterpartyId + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/cash_desk/{cashDeskId}/balance

    /* get cash_desk balance
     */
    localWapi.getCashDeskBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/cash_desk/' + params.cashDeskId + '/balance' + _urlParams(params.params), null, cb);
    };

    //loyalty && discounts

    // /company/{companyId}/loyalty/card
    /* get loyalty cards
    */
    localWapi.getLoyaltyCardList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/loyalty/card' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/loyalty/card/{loyaltyCardId}
    /* get loyalty card by id
     */
    localWapi.getLoyaltyCard = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/loyalty/card/' + params.cardId + _urlParams(params.params), null, cb);
    };
    /* update loyalty card
     */
    localWapi.updateLoyaltyCard = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/loyalty/card/' + params.cardId + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/client/{clientId}/loyalty/card/type/{cardTypeId}/enroll
    /* enroll loyalty card to client
     */
    localWapi.enrollLoyaltyCard = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/client/' + params.clientId + '/loyalty/card/type/' + params.cardTypeId + '/enroll' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/loyalty/card/type
    /* get company card type list
     */
    localWapi.getCardTypeList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/loyalty/card/type' + _urlParams(params.params), null, cb);
    };
    /* create company card type
     */
    localWapi.createCardType = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/loyalty/card/type' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/loyalty/card/type/{cardTypeId}
    /* get company card type by ID
     */
    localWapi.getCardType = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/loyalty/card/type/' + params.cardTypeId + _urlParams(params.params), null, cb);
    };
    /* update company card type
     */
    localWapi.updateCardType = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/loyalty/card/type/' + params.cardTypeId + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/discount
    /* get discounts
     */
    localWapi.calculateDiscount = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/discount' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/discount/campaign
    /* get company promotion list
     */
    localWapi.getPromotionList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/discount/campaign' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/discount/campaign/{id}/order/change"
    /* update campaign order
     */
    localWapi.promotionOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/discount/campaign/' + params.id + '/order/change', params.body, cb);
    };

    /* create company card type
     */
    localWapi.createPromotion = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/discount/campaign' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/discount/campaign/{campaignId}
    /* get company promotion by ID
     */
    localWapi.getPromotion = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/discount/campaign/' + params.campaignId + _urlParams(params.params), null, cb);
    };
    /* update company card type
     */
    localWapi.updatePromotion = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/discount/campaign/' + params.campaignId + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/client/order

    /* get company client order list
     */
    localWapi.getClientOrderList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/order' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/client/order/{orderId}

    /* get company client order
     */
    localWapi.getClientOrder = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/order/' + params.orderId + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/order

    /* get company branch client order list
     */
    localWapi.getBranchClientOrderList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/order' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/{resourceId}/client/order

    /* get company branch resource client order list
     */
    localWapi.getBranchResourceClientOrderList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/client/order' + _urlParams(params.params), null, cb);
    };

    /* create company branch client order
     */
    localWapi.createBranchClientOrder = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/branch/' + params.branchId + '/client/order' + _urlParams(params.params), params.body, cb);
    };

    /* create company branch resource client order
     */
    localWapi.createBranchResourceClientOrder = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/client/order' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/branch/{branchId}/client/order/{orderId}

    /* get company branch client order
     */
    localWapi.getBranchClientOrder = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/order/' + params.orderId + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/branch/{branchId}/resource/client/order/{orderId}

    /* get company branch resource client order
     */
    localWapi.getBranchResourceClientOrder = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/client/order/' + params.orderId + _urlParams(params.params), null, cb);
    };

    /* update company branch client order
     */
    localWapi.updateBranchClientOrder = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/branch/' + params.branchId + '/client/order/' + params.orderId + _urlParams(params.params), params.body, cb);
    };

    /* update company branch resource client order
     */
    localWapi.updateBranchResourceClientOrder = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/branch/' + params.branchId + '/resource/' + params.resourceId + '/client/order/' + params.orderId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/product/move"

    /* move products
     */
    localWapi.moveProducts = function (params, cb) {
      _request('POST', '/company/' + params.companyId +'/product/move', params.body, cb);
    };

    // "/company/{companyId}/product/{id}/order/change"
    /* update product order
     */
    localWapi.productOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/product/' + params.id + '/order/change', params.body, cb);
    };

    // "/company/{companyId}/report/cash/balance"
    /* get company cash/balance report
     */
    localWapi.getReportCashBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/report/cash/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/report/cash/flow"
    /* get company cash/flow report
     */
    localWapi.getReportCashFlow = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/report/cash/flow' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/report/cash/balance"
    /* get company branch cash/balance report
     */
    localWapi.getBranchReportCashBalance = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/report/cash/balance' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/report/cash/flow"
    /* get company branch cash/flow report
     */
    localWapi.getBranchReportCashFlow = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/report/cash/flow' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/report/cash_flow"
    /* get company resource cash_flow report
     */
    localWapi.getResourceReportCashFlow = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/resource/report/cash_flow' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/report/cash_flow"
    /* get company branch cash_flow report by resources
     */
    localWapi.getBranchResourceReportCashFlow = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/resource/report/cash_flow' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/{resourceId}/report/cash_flow"
    /* get company branch resource cash_flow report
     */
    localWapi.getOneResourceReportCashFlow = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/branch/' + params.branchId +'/resource/' + params.resourceId +'/report/cash_flow' + _urlParams(params.params), null, cb);
    };


    // "/company/{companyId}/client/retention/statistics"
    /* get company client retention statistics by resources
     */
    localWapi.getClientRetentionStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/client/retention/statistics' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/resource/client/retention/statistics"
    /* get company resource client retention statistics
     */
    localWapi.getResourceClientRetentionStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/resource/client/retention/statistics' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/client/retention/statistics"
    /* get company one branch client retention statistics
     */
    localWapi.getBranchClientRetentionStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId +'/client/retention/statistics' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/resource/client/retention/statistics"
    /* get company branch resource client retention statistics
     */
    localWapi.getBranchResourceClientRetentionStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId +'/resource/client/retention/statistics' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/finance_operation_type"
    /* get company finance_operation_type
     */
    localWapi.getFinanceOperationType = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/finance_operation_type' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/finance_operation_type/{operationTypeId}
    /* create company finance_operation_type by ID
     */
    localWapi.createFinanceOperationType = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/finance_operation_type' + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/finance_operation_type/{operationTypeId}
    /* get company finance_operation_type by ID
     */
    localWapi.getOneFinanceOperationType = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/finance_operation_type/' + params.operationTypeId + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/finance_operation_type/{operationTypeId}
    /* update company finance_operation_type by ID
     */
    localWapi.updateFinanceOperationTypeByID = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/finance_operation_type/' + params.operationTypeId + _urlParams(params.params), params.body, cb);
    };

    // /company/{companyId}/user/sign_up/phone
    /* send sign_up sms by companyId
     */
    localWapi.sendCompanySignUpByPhone = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/user/sign_up/phone/', params.body, cb);
    };

    // /user/sign_in/sms
    /* send sign_in sms
     */
    localWapi.sendSignInBySMS = function (params, cb) {
      _request('POST','/user/sign_in/sms/', params.body, cb);
    };

    // /company/{companyId}/user/reset/phone
    /* send reset password sms by companyId
     */
    localWapi.sendCompanyRecoveryPasswordByPhone = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/user/reset/phone/', params.body, cb);
    };

    // /company/{companyId}/user/sign_in/phone
    /* send sign_in sms by companyId
     */
    localWapi.sendCompanySignInByPhone = function (params, cb) {
      _request('POST','/company/' + params.companyId + '/user/sign_in/phone/', params.body, cb);
    };

    // /user/sign_in/phone/verify
    /* verify sign_in sms for user
     */
    localWapi.verifySignInByPhone = function (params, cb) {
      _request('POST','/user/sign_in/phone/verify/', params.body, cb);
    };

    // /user/sign_up/phone/verify
    /* verify sign_up sms for user
     */
    localWapi.verifySignUpByPhone = function (params, cb) {
      _request('POST','/user/sign_up/phone/verify/', params.body, cb);
    };

    // /user/reset/phone/verify
    /* verify reset password sms for user
     */
    localWapi.verifyRecoveryPasswordByPhone = function (params, cb) {
      _request('POST','/user/reset/phone/verify/', params.body, cb);
    };

    // /company/{companyId}/user/appointment
    /*create company user Appointment
     */
    localWapi.createCompanyUserAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/appointment' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/group_training/{groupTrainingId}/appointment"
    /*create company user GT appointment
     */
    localWapi.createCompanyUserGroupTrainingAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/group_training/' + params.groupTrainingId + '/appointment' + _urlParams(params.params), params.body, cb);
    };

    /* get user appointments by company
     */
    localWapi.getCompanyUserAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/user/appointment' + _urlParams(params.params), null, cb);
    };

    // /company/{companyId}/user/appointment/{appointmentId}
    /* get user appointment by company
     */
    localWapi.getOneCompanyUserAppointment = function (params, cb) {
      _request('GET', '/company/' + params.companyId +'/user/appointment/' + params.appointmentId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/user/appointment/{appointmentId}/cancel"
    /*cancel user appointment by company
     */

    localWapi.cancelCompanyUserAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/appointment/' + params.appointmentId + '/cancel' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/appointment/{appointmentId}"
    /*update user appointment by company
     */

    localWapi.updateCompanyUserAppointment = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/appointment/' + params.appointmentId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/survey/configuration"

    localWapi.createSurveyForm = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/survey/configuration' + _urlParams(params.params), params.body, cb);
    };

    localWapi.getSurveyFormList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey/configuration' + _urlParams(params.params), null, cb);
    };

    localWapi.getSurveyForm = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey/configuration/' + params.id + _urlParams(params.params), null, cb);
    };

    localWapi.updateSurveyForm = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/survey/configuration/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey/configuration"

    localWapi.getBranchSurveyFormList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/configuration' + _urlParams(params.params), null, cb);
    };

    localWapi.getBranchSurveyForm = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/configuration/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/survey_notification"

    localWapi.getSurveyNotificationList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey_notification' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey_notification"

    localWapi.createBranchSurveyNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey_notification' + _urlParams(params.params), params.body, cb);
    };

    localWapi.getBranchSurveyNotificationList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey_notification' + _urlParams(params.params), null, cb);
    };

    localWapi.getBranchSurveyNotification = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey_notification/' + params.id + _urlParams(params.params), null, cb);
    };

    localWapi.updateBranchSurveyNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey_notification/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey_notification/clone"

    localWapi.cloneBranchSurveyNotification = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey_notification/clone', params.body, cb);
    };

    // "/company/{companyId}/survey"

    localWapi.getSurveyResultsList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/survey/{surveyId}"

    localWapi.getSurveyResult = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey/' + params.id + _urlParams(params.params), null, cb);
    };

    localWapi.updateSurveyResult = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/survey/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey"

    localWapi.getBranchSurveyResultsList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey/{surveyId}"

    localWapi.getBranchSurveyResult = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/' + params.id + _urlParams(params.params), null, cb);
    };

    localWapi.updateBranchSurveyResult = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/' + params.id + _urlParams(params.params), params.body, cb);
    };

    /*survey statistics
     */
    // "/company/{companyId}/branch/{branchId}/survey/statistics";

    localWapi.getBranchSurveyStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/statistics/' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/survey/configuration/{surveyConfigurationId}/statistics";

    localWapi.getBranchSurveyConfigurationStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/survey/configuration/' + params.surveyConfigurationId + '/statistics' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/survey/statistics";

    localWapi.getSurveyStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey/statistics/' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/survey/configuration/{surveyConfigurationId}/statistics";

    localWapi.getSurveyConfigurationStatistics = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/survey/configuration/' + params.surveyConfigurationId + '/statistics' + _urlParams(params.params), null, cb);
    };

    // "/user/survey";

    localWapi.getUserSurveyList = function (params, cb) {
      _request('GET', '/user/survey' + _urlParams(params.params), null, cb);
    };

    // "/user/survey/{surveyId}";

    localWapi.getUserSurvey = function (params, cb) {
      _request('GET', '/user/survey/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/user/company/{companyId}/survey"

    localWapi.getCompanyUserSurveyList = function (params, cb) {
      _request('GET', '/user/company/' + params.companyId + '/survey' + _urlParams(params.params), null, cb);
    };

    // "/user/company/{companyId}/survey/{surveyId}"

    localWapi.getCompanyUserSurvey = function (params, cb) {
      _request('GET', '/user/company/' + params.companyId + '/survey/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/user/company/{companyId}/rating"

    localWapi.getCompanyUserRatingList = function (params, cb) {
      _request('GET', '/user/company/' + params.companyId + '/rating' + _urlParams(params.params), null, cb);
    };

    // "/user/company/{companyId}/rating/{ratingId}"

    localWapi.getCompanyUserRating = function (params, cb) {
      _request('GET', '/user/company/' + params.companyId + '/rating/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/user/rating"

    localWapi.getUserRatingList = function (params, cb) {
      _request('GET', '/user/rating' + _urlParams(params.params), null, cb);
    };

    // "/user/rating/{ratingId}"

    localWapi.getUserRating = function (params, cb) {
      _request('GET', '/user/rating/' + params.id + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/client/blocker/rule"

    localWapi.getCompanyClientBlockerRuleList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/blocker/rule' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/client/blocker/rule/{ruleId}"

    localWapi.getCompanyClientBlockerRule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/client/blocker/rule/' + params.ruleId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/client/blocker/rule";

    localWapi.getBranchClientBlockerRuleList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/blocker/rule' + _urlParams(params.params), null, cb);
    };

    localWapi.createBranchClientBlockerRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/blocker/rule' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/client/blocker/rule/{ruleId}";

    localWapi.getBranchClientBlockerRule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/blocker/rule/' + params.ruleId + _urlParams(params.params), null, cb);
    };

    localWapi.updateBranchClientBlockerRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/blocker/rule/' + params.ruleId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/client/blocker/rule/clone"

    localWapi.cloneBranchClientBlockerRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/client/blocker/rule/clone', params.body, cb);
    };

    // "/company/{companyId}/entity_filter"

    localWapi.getCompanyClientEntityFilterList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/entity_filter' + _urlParams(params.params), null, cb);
    };

    localWapi.createClientEntityFilter = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/entity_filter' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/entity_filter/{id}"

    localWapi.getCompanyClientEntityFilter = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/entity_filter/' + params.id + _urlParams(params.params), null, cb);
    };

    localWapi.updateClientEntityFilter = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/entity_filter/' + params.id + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tag"

    localWapi.getTagList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tag' + _urlParams(params.params), null, cb);
    };

    localWapi.createTag = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tag' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/tag/{tagId}"

    localWapi.getTag = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/tag/' + params.tagId + _urlParams(params.params), null, cb);
    };

    localWapi.updateTag = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/tag/' + params.tagId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/client/{clientId}/ban"

    localWapi.banClient = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/client/' + params.clientId + '/ban' + _urlParams(params.params), params.body, cb);
    };

    localWapi.unbanClient = function (params, cb) {
      _request('DELETE', '/company/' + params.companyId + '/client/' + params.clientId + '/ban' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/payment/rule"

    localWapi.getPaymentRulesList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment/rule' + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/payment/rule/{ruleId}"

    localWapi.getPaymentRule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/payment/rule/' + params.ruleId + _urlParams(params.params), null, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payment/rule"

    localWapi.getBranchPaymentRulesList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/rule' + _urlParams(params.params), null, cb);
    };

    localWapi.createBranchPaymentRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/rule' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payment/rule/{ruleId}"

    localWapi.getBranchPaymentRule = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/rule/' + params.ruleId + _urlParams(params.params), null, cb);
    };

    localWapi.updateBranchPaymentRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/rule/' + params.ruleId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/branch/{branchId}/payment/rule/clone"

    localWapi.cloneBranchPaymentRule = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/branch/' + params.branchId + '/payment/rule/clone', params.body, cb);
    };

    // "/client/appointment/calculate/price"

    localWapi.calculateAppointmentPrice = function (params, cb) {
      _request('POST', '/client/appointment/calculate/price' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/task"
    localWapi.getUserTaskList = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user/task' + _urlParams(params.params), null, cb);
    };
    localWapi.createUserTask = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/task' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/task/{taskId}"
    localWapi.getUserTask = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user/task/' + params.taskId + _urlParams(params.params), null, cb);
    };
    localWapi.updateUserTask = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/task/' + params.taskId + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/task/{taskId}/order/change"
    localWapi.userTaskOrderChange = function (params, cb) {
      _request('POST', '/company/' + params.companyId + '/user/task/' + params.taskId + '/order/change' + _urlParams(params.params), params.body, cb);
    };

    // "/company/{companyId}/user/task/collaborator"
    localWapi.getUserTaskCollaborator = function (params, cb) {
      _request('GET', '/company/' + params.companyId + '/user/task/collaborator' + _urlParams(params.params), null, cb);
    };

    return localWapi;
  };
  return Wapi;
}));
